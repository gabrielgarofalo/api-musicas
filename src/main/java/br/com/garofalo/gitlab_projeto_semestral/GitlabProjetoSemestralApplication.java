package br.com.garofalo.gitlab_projeto_semestral;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabProjetoSemestralApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitlabProjetoSemestralApplication.class, args);
	}

}
