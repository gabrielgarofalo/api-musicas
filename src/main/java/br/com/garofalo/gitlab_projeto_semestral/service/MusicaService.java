package br.com.garofalo.gitlab_projeto_semestral.service;

import br.com.garofalo.gitlab_projeto_semestral.model.Musica;
import br.com.garofalo.gitlab_projeto_semestral.repository.MusicaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;

@Service
public class MusicaService {
    @Autowired
    private MusicaRepository musicaRepository;

    public Musica salvar(Musica musica) {
        Musica existingMusica = this.musicaRepository.findByNome(musica.getNome());
        if (existingMusica != null) {
            return existingMusica;
        }
        musica.setAvaliacao(0);
        return this.musicaRepository.save(musica);
    }

    public List<Musica> listar() {
        List<Musica> musicas = this.musicaRepository.findAll();
        musicas.sort(Comparator.comparing(Musica::getAvaliacao).reversed().thenComparing(Musica::getNome));
        return musicas;
    }

    public Musica atualizar(String nome, int avaliacao) {
        if(avaliacao < 0 || avaliacao > 5) {
            throw new IllegalArgumentException("Avaliação deve estar entre 0 e 5.");
        }

        Musica musica = this.musicaRepository.findByNome(nome);

        if(musica == null) {
            throw new IllegalArgumentException("Nenhuma música encontrada com o nome: " + nome);
        }

        musica.setAvaliacao(avaliacao);
        return this.musicaRepository.save(musica);
    }

}