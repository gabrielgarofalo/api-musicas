package br.com.garofalo.gitlab_projeto_semestral.model;

import lombok.*;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@NoArgsConstructor
//construtor com todos os parâmetros, na ordem de declaração
@AllArgsConstructor
//quando aplicados a uma classe
//geram getters/setters para todos os campos não marcados como
@Getter
@Setter
@Entity
public class Musica {
    @Id
    @GeneratedValue
    private Long id;
    private String nome;
    private int avaliacao;
}

