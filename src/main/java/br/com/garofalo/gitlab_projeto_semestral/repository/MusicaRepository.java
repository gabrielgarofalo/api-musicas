package br.com.garofalo.gitlab_projeto_semestral.repository;

import br.com.garofalo.gitlab_projeto_semestral.model.Musica;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MusicaRepository extends JpaRepository<Musica, Long> {
    Musica findByNome(String nome);
}
