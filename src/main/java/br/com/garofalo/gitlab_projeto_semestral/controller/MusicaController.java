package br.com.garofalo.gitlab_projeto_semestral.controller;

import br.com.garofalo.gitlab_projeto_semestral.model.Musica;
import br.com.garofalo.gitlab_projeto_semestral.service.MusicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/musicas")
public class MusicaController {
    @Autowired
    private MusicaService musicaService;

    @PostMapping("/salvar")
    public ResponseEntity<Musica> salvar(@RequestBody Musica musica) {
        Musica savedMusica = this.musicaService.salvar(musica);
        return ResponseEntity.ok(savedMusica);
    }

    @GetMapping
    public List<Musica> listar() {
        return this.musicaService.listar();
    }

    @PutMapping("/atualizar")
    public ResponseEntity<Musica> atualizar(@RequestParam String nome, @RequestParam int avaliacao) {
        Musica updatedMusica = this.musicaService.atualizar(nome, avaliacao);
        return ResponseEntity.ok(updatedMusica);
    }
}
