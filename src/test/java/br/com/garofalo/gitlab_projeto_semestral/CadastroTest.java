package br.com.garofalo.gitlab_projeto_semestral;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CadastroTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testCadastrarMusica() throws Exception {
        String musicaJson = "{\"nome\": \"Nova Musica\"}";

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/musicas/salvar")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(musicaJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.avaliacao").value(0));
    }
}
